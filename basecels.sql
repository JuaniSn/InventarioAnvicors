-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-08-2018 a las 23:24:25
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `basecels`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anaquel`
--

CREATE TABLE `anaquel` (
  `id_anaquel` int(11) NOT NULL,
  `nro_anaquel` varchar(10) NOT NULL,
  `sec_anaquel` varchar(10) NOT NULL,
  `col_anaquel` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `anaquel`
--

INSERT INTO `anaquel` (`id_anaquel`, `nro_anaquel`, `sec_anaquel`, `col_anaquel`) VALUES
(1, 'Anaquel 1', 'Seccion A', 'Bloque 1'),
(2, 'Anaquel 1', 'Seccion A', 'Bloque 2'),
(3, 'Anaquel 1', 'Seccion A', 'Bloque 3'),
(4, 'Anaquel 1', 'Seccion B', 'Bloque 1'),
(5, 'Anaquel 1', 'Seccion B', 'Bloque 2'),
(6, 'Anaquel 1', 'Seccion B', 'Bloque 3'),
(7, 'Anaquel 1', 'Seccion C', 'Bloque 1'),
(8, 'Anaquel 1', 'Seccion C', 'Bloque 2'),
(9, 'Anaquel 1', 'Seccion C', 'Bloque 3'),
(10, 'Anaquel 2', 'Seccion A', 'Bloque 1'),
(11, 'Anaquel 2', 'Seccion A', 'Bloque 2'),
(12, 'Anaquel 2', 'Seccion A', 'Bloque 3'),
(13, 'Anaquel 2', 'Seccion B', 'Bloque 1'),
(14, 'Anaquel 2', 'Seccion B', 'Bloque 2'),
(15, 'Anaquel 2', 'Seccion B', 'Bloque 3'),
(16, 'Anaquel 2', 'Seccion C', 'Bloque 1'),
(17, 'Anaquel 2', 'Seccion C', 'Bloque 2'),
(18, 'Anaquel 2', 'Seccion C', 'Bloque 3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id_articulo` int(4) NOT NULL,
  `id_categoria` int(4) NOT NULL,
  `a_nombre` varchar(100) NOT NULL,
  `a_descripcion` text,
  `a_fecha_agregado` datetime NOT NULL,
  `a_cantidad` int(11) NOT NULL,
  `a_precio_venta` varchar(50) NOT NULL DEFAULT '---BsS',
  `id_anaquel` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id_articulo`, `id_categoria`, `a_nombre`, `a_descripcion`, `a_fecha_agregado`, `a_cantidad`, `a_precio_venta`, `id_anaquel`) VALUES
(8, 101, 'SAMSUNG MINI S3', 'blablabla', '2018-08-14 00:01:22', 12, '200.000.000', 4),
(9, 102, 'MOTO E SEGUNDA GENERACION', 'sadjkhasd', '2018-08-14 20:27:28', 1231, '231.234.234.234.234', 1),
(10, 102, 'HUAWEI SNAPDRAGON', 'sadasdsa', '2018-08-14 20:28:17', 33123, '4.234.234', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos_colores`
--

CREATE TABLE `articulos_colores` (
  `id_artcolor` int(4) NOT NULL,
  `id_articulo` int(4) NOT NULL,
  `id_color` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articulos_colores`
--

INSERT INTO `articulos_colores` (`id_artcolor`, `id_articulo`, `id_color`) VALUES
(6, 8, 12),
(7, 9, 12),
(8, 10, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `c_nombre` varchar(50) NOT NULL,
  `c_descripcion` text NOT NULL,
  `c_fecha_agregado` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `c_nombre`, `c_descripcion`, `c_fecha_agregado`, `estado`) VALUES
(101, 'TELEFONOS', 'aaa', '2018-08-13 21:01:47', 1),
(102, 'ACCESORIOS', 'aaaa', '2018-08-13 21:01:55', 1),
(103, 'REMBERTO', 'marico', '2018-08-13 21:02:12', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `id_color` int(11) NOT NULL,
  `ncolor` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`id_color`, `ncolor`) VALUES
(1, 'NEGRO'),
(2, 'BLANCO'),
(3, 'PLATEADO'),
(4, 'DORADO'),
(5, 'AZUL'),
(6, 'VERDE'),
(7, 'ROJO'),
(8, 'ROSADO'),
(9, 'AMARILLO'),
(10, 'NARANJA'),
(11, 'MORADO'),
(12, 'VINOTINTO'),
(13, 'AQUAMARINA'),
(14, 'CELESTE'),
(15, 'LAPIZLAZULI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_prov_articulo`
--

CREATE TABLE `lista_prov_articulo` (
  `id_prov_art` int(4) NOT NULL,
  `id_proveedor` int(4) NOT NULL,
  `id_articulo` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `lista_prov_articulo`
--

INSERT INTO `lista_prov_articulo` (`id_prov_art`, `id_proveedor`, `id_articulo`) VALUES
(6, 41, 8),
(7, 41, 9),
(8, 41, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `p_nombre` varchar(50) NOT NULL,
  `p_telefono_cel` int(11) DEFAULT NULL,
  `p_telefono_ofi` int(11) DEFAULT NULL,
  `p_correo` varchar(50) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `p_nombre`, `p_telefono_cel`, `p_telefono_ofi`, `p_correo`, `estado`) VALUES
(41, 'VENEZUELA TECNOLOGY C.A', 416135544, 2126821441, 'ven_tecnology@gmail.com', 1),
(42, 'VIT CANAMY C.A', 0, 2121333333, 'vit@gob.com.ve', 0),
(43, 'BRUDER APSA', 2147483647, 0, '', 0),
(44, 'JUANIXD', 0, 0, '', 0),
(45, 'AAA', 0, 0, '', 0),
(46, 'CHHH', 0, 0, '', 0),
(47, 'JADITA LA HUERFANITA', 0, 0, '', 0),
(48, 'DOWNLOAD A 1.C', 0, 0, '', 0),
(49, 'ASDAS', 0, 0, 'snjuank@gmail.com', 0),
(50, 'DASDDAS', 0, 0, '', 0),
(51, 'DSASDASD', 0, 2147483647, 'asdsadqwewqe@gmail.com', 0),
(52, 'ASDAS', 0, 0, '', 0),
(53, 'ASDAS', 0, 0, '', 0),
(54, 'XDDD', 0, 0, '', 0),
(55, ':V', 0, 0, '', 0),
(56, 'ASDASDASD', 13213, 23223, 'snsfsadasdas', 1),
(57, 'XD', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `clave` varchar(30) NOT NULL,
  `u_tipo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `clave`, `u_tipo`) VALUES
(1, 'usuario', '1234', 'administrador');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_listar_articulo`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `v_listar_articulo` (
`id_articulo` int(4)
,`a_nombre` varchar(100)
,`c_nombre` varchar(50)
,`a_descripcion` text
,`a_fecha_agregado` datetime
,`a_cantidad` int(11)
,`a_precio_venta` varchar(50)
,`anaquel` varchar(34)
,`p_nombre` varchar(50)
,`id_proveedor` int(11)
,`ncolor` varchar(40)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_listar_articulo`
--
DROP TABLE IF EXISTS `v_listar_articulo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_listar_articulo`  AS  select `articulos`.`id_articulo` AS `id_articulo`,`articulos`.`a_nombre` AS `a_nombre`,`categorias`.`c_nombre` AS `c_nombre`,`articulos`.`a_descripcion` AS `a_descripcion`,`articulos`.`a_fecha_agregado` AS `a_fecha_agregado`,`articulos`.`a_cantidad` AS `a_cantidad`,`articulos`.`a_precio_venta` AS `a_precio_venta`,concat(`anaquel`.`nro_anaquel`,', ',`anaquel`.`sec_anaquel`,', ',`anaquel`.`col_anaquel`) AS `anaquel`,`proveedores`.`p_nombre` AS `p_nombre`,`proveedores`.`id_proveedor` AS `id_proveedor`,`colores`.`ncolor` AS `ncolor` from ((((((`articulos` join `lista_prov_articulo` on((`articulos`.`id_articulo` = `lista_prov_articulo`.`id_articulo`))) join `proveedores` on((`lista_prov_articulo`.`id_proveedor` = `proveedores`.`id_proveedor`))) join `categorias` on((`categorias`.`id_categoria` = `articulos`.`id_categoria`))) join `anaquel` on((`anaquel`.`id_anaquel` = `articulos`.`id_anaquel`))) join `articulos_colores` on((`articulos_colores`.`id_articulo` = `articulos`.`id_articulo`))) join `colores` on((`colores`.`id_color` = `articulos_colores`.`id_color`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anaquel`
--
ALTER TABLE `anaquel`
  ADD PRIMARY KEY (`id_anaquel`);

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id_articulo`),
  ADD KEY `id_anaquel` (`id_anaquel`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `articulos_colores`
--
ALTER TABLE `articulos_colores`
  ADD PRIMARY KEY (`id_artcolor`),
  ADD KEY `id_articulo` (`id_articulo`),
  ADD KEY `id_color` (`id_color`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`id_color`);

--
-- Indices de la tabla `lista_prov_articulo`
--
ALTER TABLE `lista_prov_articulo`
  ADD PRIMARY KEY (`id_prov_art`),
  ADD KEY `id_proveedor` (`id_proveedor`),
  ADD KEY `id_articulo` (`id_articulo`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anaquel`
--
ALTER TABLE `anaquel`
  MODIFY `id_anaquel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id_articulo` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `articulos_colores`
--
ALTER TABLE `articulos_colores`
  MODIFY `id_artcolor` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `id_color` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `lista_prov_articulo`
--
ALTER TABLE `lista_prov_articulo`
  MODIFY `id_prov_art` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD CONSTRAINT `articulos_ibfk_1` FOREIGN KEY (`id_anaquel`) REFERENCES `anaquel` (`id_anaquel`),
  ADD CONSTRAINT `articulos_ibfk_2` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`);

--
-- Filtros para la tabla `articulos_colores`
--
ALTER TABLE `articulos_colores`
  ADD CONSTRAINT `articulos_colores_ibfk_1` FOREIGN KEY (`id_color`) REFERENCES `colores` (`id_color`),
  ADD CONSTRAINT `articulos_colores_ibfk_2` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id_articulo`) ON DELETE CASCADE;

--
-- Filtros para la tabla `lista_prov_articulo`
--
ALTER TABLE `lista_prov_articulo`
  ADD CONSTRAINT `lista_prov_articulo_ibfk_1` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id_articulo`) ON DELETE CASCADE,
  ADD CONSTRAINT `lista_prov_articulo_ibfk_2` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
