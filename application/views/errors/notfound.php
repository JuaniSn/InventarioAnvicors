<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="<?= base_url()  ?>public/img/icon.png">
	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/Bootstrap/css/bootstrap.min.css">
	<title>La pagina no existe</title>
</head>
<body>
	<div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-md-12">
                  <div class="box">
                    <h1>La pagina que intentas acceder, no existe. </h1>
                    <a href="<?= base_url()  ?>panel">Regresar al Inicio</a>
                  </div>
              </div>
       </section>

    </div>
</body>
</html>