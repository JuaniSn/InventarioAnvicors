<body class="skin-green">
  <!-- Content-Wrapper para el contenedor que tendra el contenido -->
      <div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-md-12">
                  <div class="box">
                        <div class="container">
                            <h1 class="text-center">INFORMACIÓN DEL SISTEMA</h1><br>
                        
                              <div class="row">
                                <div class="col-md-2"></div>  
                                      <div class="col-md-8 text-justify" >

                                          <h5 class="font-weight-bold">Desarrollado en: </h5>
                                          <h5 class="font-italic">Sistema basado en Codeigniter 3, Bootstrap 4, Jquery, Alertify(Plugin de Jquery), Datatable (Plugin de Jquery), Javascript, PHP7 y utiliza un motor de base de datos MariaDB. </h5>
                                          <br>
                                          <h5 class="font-weight-bold">Caracteristicas </h5>
                                          <h5 class="font-italic">
                                            <ul><b style="color: teal">Panel:</b> Inicio con un dashboard de navegación y opción para cambiar la interfaz del sistema</ul> 

                                            <ul><b style="color: teal">Almacén</b> Tiene dos opciones: 
                                              <br>
                                              <li>Categorías: En esta sección se identifican los tipos de artículos que se tienen el inventario para administrar de mejor manera entre accesorios, teléfonos y demás tipos de artículos que se puedan introducir en la tienda. Se puede visualizar la lista de categorías en una tabla y opciones para agregar, editar y eliminar.</li>
                                              <br>
                                              <li>Artículos: En esta sección se enlistan en una tabla con busqueda en tiempo real todos los artículos dentro de los anaqueles. Tiene las respectivas opciones de modificar, consultar a detalle cada artículo y agregar nuevos.</li> </ul>

                                              <ul><b style="color: teal">Proveedor: </b>Lista de proveedores en una tabla. Tiene las opciones de consultar una lista de artículos vendidos por el mismo y las mismas de: Agregar, editar y eliminar.</ul>

                                              <ul><b style="color: teal">Administrador: </b>Opciones sobre el sistema: <br><br>
                                              <li>Usuario: Opción para cambiar el usuario y contraseña para acceder al sistema.</li>
                                              <br>
                                              <li>Respaldar BD: Exporta un archivo SQL como backup de la base de datos</li>
                                            </ul>
                                             </h5>

                                             <br><br><br>

                                             <p class="text-center">Sistema desarrollado por: Juan Reyes & Remberto Lidueña.</p>

                                    </div> 
                                <div class="col-md-2"></div> 
                              </div>
                           

                        </div>
                  </div>
              </div>
            </div>
       </section>

    </div>

<!-- FIN CONTENIDO DE PAGINA -->