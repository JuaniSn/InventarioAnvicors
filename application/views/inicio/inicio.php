<body class="skin-green">
  <!-- Content-Wrapper para el contenedor que tendra el contenido -->
      <div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?= $cantArticulos  ?></h3>

                        <p>Articulos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tablet"></i>
                    </div>
                    <a href="<?= base_url() ?>panel/page/articulos" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $cantCategorias  ?></h3>

                <p>Categorias</p>
            </div>
            <div class="icon">
                <i class="fa fa-tags"></i>
            </div>
            <a href="<?= base_url() ?>panel/page/categorias" class="small-box-footer">Mas información<i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= $cantProveedores  ?></h3>

                <p>Proveedores</p>
            </div>
            <div class="icon">
                <i class="fa fa-truck"></i>
            </div>
            <a href="<?= base_url() ?>panel/page/proveedores" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>1</h3>

                <p>Usuario</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-plus"></i>
            </div>
            <a onclick="dialog()" class="small-box-footer"> Editar Usuario <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
</section>
</div>