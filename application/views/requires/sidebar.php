
      <!-- SIDEBAR, MENU VERTICAL A LA IZQUIERDA (Contiene el LOGOTIPO) -->
      <aside class="main-sidebar">
        <!-- Se pone otra vez sidebar para poner en funcionamiento su menu  -->
        <section class="sidebar" >      
          <!-- Lista de elementos en el menu -->
          <ul class="sidebar-menu">
            <!-- Se deja vacío este li para hacer un espaciado entre el logotipo y los elementos -->
              <li class="header">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">

                          <div class="carousel-item active">
                            <img class="d-block w-100" src=" <?= base_url() ?>public/img/2.jpg " alt="First slide" with="190px" height="190px">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src=" <?= base_url() ?>public/img/3.jpg " alt="First slide" with="190px" height="190px">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src=" <?= base_url() ?>public/img/4.jpg " alt="First slide" with="190px" height="190px">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src=" <?= base_url() ?>public/img/5.jpg " alt="First slide" with="190px" height="190px">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src=" <?= base_url() ?>public/img/6.jpg " alt="First slide" with="190px" height="190px">
                          </div>
                          <div class="carousel-item">
                            <img class="d-block w-100" src=" <?= base_url() ?>public/img/7.jpg " alt="First slide" with="190px" height="190px">
                          </div>
                  </div>
                </div>
              </li>

              <li>
                <a href="<?= base_url()  ?>panel">
                  <i class="fa fa-tasks"></i> <span>Panel</span>
                </a>
              </li>  

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-laptop"></i><span>Almacén</span>
                  <i class="fa unik fa-angle-left pull-right"></i>
                </a>
                  <ul class="treeview-menu">
                    <li>
                      <a href="<?= base_url()  ?>panel/page/articulos"><i class="fa fa-tablet"></i> Artículos</a>
                    </li>
                    <li>
                      <a href="<?= base_url()  ?>panel/page/categorias"><i class="fa fa-tags"></i> Categorías</a>
                    </li>
                  </ul>
              </li>
              
              <li class="treeview">
                <a href="<?= base_url()  ?>panel/page/proveedores">
                  <i class="fa fa-truck"></i><span>Proveedor</span>
                </a>
              </li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-cog"></i><span>Administrador</span>
                   <i class="fa fa-angle-left pull-right"></i>
                </a>
                  <ul class="treeview-menu">
                    <li>
                      <a onclick="dialog()"><i class="fa fa-group"></i> Usuario </a>
                    </li>
                    <li>
                      <a href="<?= base_url()  ?>panel/page/respaldar"><i class="fa fa-clone"></i> Respaldar BD</a>
                    </li>
                  </ul>
              </li>                       
             
              <li>
                <a href="<?= base_url()  ?>panel/page/info">
                  <i class="fa fa-info-circle"></i> <span>Información</span>
                </a>
              </li>
                          
          </ul>

        </section>

      </aside>