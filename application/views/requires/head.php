<!DOCTYPE html>
<html>
<head>  


  <!-- METAS -->

	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <title><?= $title  ?></title>


	<!-- Icono -->

	<link rel="shortcut icon" href="<?= base_url()  ?>public/img/icon.png">

	<!-- Font Awesome (iconos) -->

	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/Bootstrap/fonts/fonts/font-awesome.css">

	<!-- Bootstrap 4.1 -->
	<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/Bootstrap/css/bootstrap.min.css">

	<!-- Bootstrap 3.3.5 -->

    <!-- <link rel="stylesheet" href="<?= base_url()  ?>public/adminLTE/bootstrap.min.css"> -->

	<!-- ADMIN LTE -->

	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/adminLTE/AdminLTE.min.css">
	
	<!-- SKINS ALL -->

	<link rel="stylesheet" href="<?= base_url()  ?>public/adminLTE/_all-skins.min.css">

	<!-- Estilos propios de CSS -->
	
	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/css/estilos_propios.css">

	<!-- DATABLES de JQUERY -->

	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/datatable/tables/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/datatable/buttons/css/buttons.dataTables.min.css" />

    <!-- ALERTIFY PARA MODALES -->

    <link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/alertifyjs/css/themes/default.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/alertifyjs/css/themes/default.css">

	<!-- Sweal -->

	<link rel="stylesheet" type="text/css" href="<?= base_url()  ?>public/Bootstrap/css/sweetalert.css">

	
</head>