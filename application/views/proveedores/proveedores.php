<body class="skin-green" onload="loadtable()">

  <!-- Content-Wrapper para el contenedor que tendra el contenido -->
      <div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-md-12">
                  <div class="box">
                      <!-- ENCABEZADO DE LA CAJA  -->
                      <div class="box-header bg1">
                        <button class="btn bt1 pull-right" data-toggle="modal" onclick="add_proveedor()"><i class="fa fa-plus-circle "></i> Agregar</button>
                            <h3><i class="fa fa-truck"></i> Proveedores </h3>
                      
                      </div>
                      
                      <!-- Centro de la caja  -->
                      <div class="panel-body table-responsive tableresp">
                        <div class="container">
                          <hr>
        
                              <table id="tb_proveedor" class="table table-hover table-bordered table-striped">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Número 1</th>
                                        <th>Número 2</th>
                                        <th>Correo Electronico</th>
                                        <th>Artículos</th>
                                        <th>Opciones</th>
                                     </tr>

                                </thead>
                                 <tbody id="tbody"> 
                                 <!-- 	<tr>
                                 		<td>Task Phone C.A</td>
                                 		<td>04261445597</td>
                                 		<td>Task@gmail.com</td>
                                 		<td>15/07/2018 10:22:33</td>
                                 		<td><button class="btn btn-success" data-toggle="modal" data-target="#enlistararticulos">Lista</button></td>
                                 		<td><button class="btn btn-warning" data-toggle="modal" data-target="#editarproveedor">Editar</button> <button class="btn btn-danger">Eliminar</button></td>
                                 	</tr> -->
                                 </tbody>

                              </table>
                              <hr>
                        </div>
                    
                  </div>
              </div>
          </div>
        </div>
      </section>

    </div>

<!-- FIN CONTENIDO DE PAGINA  -->

<?php require "modal/modalProveedores.php" ?>