<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?= $title ?></title>

  </head>
  <body class="view-login">
  
      <div class="contenedor-login">
        <h2 class="titulo-sesion">Sistema Anvicor's</h2>
        <!-- <img src="public/img/logoanvi-blanco.png"> -->
        <hr class="line-sesion">

        <div class="formulario-login">
              <div class="formulario-grupo">
                  <i class="icono izquierda fa fa-user"></i> 
                  <input type="text" name="usuario" class="usuario" placeholder="Usuario" id="usuario" onclick="quitarspan()">
              </div>

              <div class="formulario-grupo">
                  <i class="icono izquierda fa fa-lock"></i> 
                  <input type="password" name="clave" class="clave" placeholder="Contraseña" id="clave" onclick="quitarspan()">
                 <i class="submit-btn fa fa-arrow-right" onclick="validation()" title="Ingresar"></i>
              </div>

              <?php if($advertencia == 1){   ?>
              <span id="spanError2">La contraseña o el usuario no coinciden</span>
              <?php }  ?>

              <?php if($advertencia == 2){   ?>
              <span id="spanError2">Debe iniciar sesion</span>
              <?php }  ?>

        </div>
        
    </div>

     <script src="<?= base_url()  ?>public/jquery/jquery-3.3.1.min.js"></script>
     <script src="<?= base_url()  ?>public/Bootstrap/js/bootstrap.min.js"></script>
     <script src="<?= base_url()  ?>public/adminLTEjs/app.min.js"></script>
     <script src="<?= base_url()  ?>public/js/login.js"></script>



  </body>
</html>
