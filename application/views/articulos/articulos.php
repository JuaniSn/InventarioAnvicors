<div  id="prueba">
    <body class="skin-green" id="body" onload="cargartbArticulo()">
      
        <!-- Content-Wrapper para el contenedor que tendra el contenido -->
          <div class="content-wrapper">
            
            <!-- contenido -->
            <section class="content">
              <!-- Una fila -->
                <div class="row"">
                  <!-- 12 columnas -->
                  <div class="col-md-12">
                      <div class="box">
                        <!-- ENCABEZADO DE LA CAJA  -->
                        <div class="box-header bg1">
                          <button class="btn bt1 pull-right" onclick="add_articulo()"><i class="fa fa-plus-circle"></i> Agregar</button>
                              <h3><i class="fa fa-tablet"></i>  Artículos </h3> 
                        
                        </div>
                        
                        <!-- Centro de la caja  -->
                        <div class="panel-body table-responsive tableresp">
                          <div class="container">
                            <hr>
                              <table id="tb_articulo" class="table table-striped table-bordered table-condensed table-hover" >
                                 <thead class="thead-dark">
                                   <tr>
                                      <th>Serial</th>
                                      <th>Nombre</th>
                                      <th>Categoria</th>
                                      <th>Cantidad</th>
                                      <th>Precio</th>
                                      <!-- <th>Anaquel</th> -->
                                      <th>Acción</th>
                                  
                                   </tr>

                                </thead>
                                 <tbody>
                                       
                                 </tbody>


                               </table>
                               <hr>
                        </div>
                        
                      </div>
                  </div>
              </div>
          </section>

        </div>
</div>

<!-- FIN CONTENIDO DE PAGINA  -->

    
<?php require "modal/modalArticulos.php" ?>


<!--
<script  type="text/javascript"">
  
  function cargar(){
    alert("asd");
    var link = "http://localhost/InventarioAnvicors/";
    var recibe;
    $.ajax({
      url: link+"Panel/cargarArticulosInicial",
      type: 'POST',
      dataType: 'JSON',
      success: function(data){
          var a = data;
          var y="<tr>";
         
          $("#tb_articulo").DataTable().clear()
          $("#tb_articulo").DataTable({
              "autoWidth": false,
              destroy: true,
          //  'data':data,
          
             "columns":[
              {'data':"nombre"},
              {'data':"id"}
              
          ],
          
          
          "columnDefs":[
              {
                
                "defaultContent":"<i>-aaa-</i>"
              }
          ]
         
          
        });
        
   //   } 
       
         //   $("#tb_articulo").DataTable().clear();
           console.log(a);
          $.each(a,function(index,item){
            var a= null;
           console.log(item);
             $("#tb_articulo").DataTable().row.add(item).draw();
            //a=index
         // }
         
          });
            $("#tb_articulo").DataTable().autoWidth(true);
         

     // }
    }


  });
}

  
</script>
-->