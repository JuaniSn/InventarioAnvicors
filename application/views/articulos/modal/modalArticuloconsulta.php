
<!-- MODAL PARA MODIFICAR UN ARTICULO -->

    <div class="modal fade" id="editararticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Editar Artículo  </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="limpiar()">&times;</span></button> 
          </div>

          <div class="modal-body bg-body-modal">
            
               <b><label for="anombre">Nombre</label></b>
                <input type="text" name="anombre" id="n1" placeholder="Marca y Modelo" class="form-control input-sm limpiar">

               <div class="div-select">
                  <b><label>Categoria: </label></b>
                   <select id="seleccion" name="telefono" class="form-control">
                      <option value="0">Seleccione</option>
                      <option >Telefono</option>
                   </select>
                </div>

               <b><label for="Precio">Precio para venta</label></b>
                <input type="text" id="precio" name="Precio" placeholder="Precio" class="form-control input-sm limpiar">

                <b> <label for="adetalle">Detalle</label></b>
                <textarea id="detalle" name="adetalle" placeholder="Ejemplo: 'Actualizacion de precio: de XXX bsf a XXX bsf o si solo modificó el nombre'" class="form-control input-sm limpiar" rows="2" cols="50"></textarea>

          </div>

          <div class="modal-footer bg-footer-modal">
            <button class="btn btn-warning"  onclick='actualizar()' >Actualizar</button>
          </div>
        </div>
      </div>
    </div>
         
<!--  FIN MODAL MODIFICAR UN ARTICULO -->


<!-- MODAL PARA CARGAR UN ARTICULO -->

    <div class="modal fade" id="cargararticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cloud-upload"></i> Cargar Artículo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="limpiar()">&times;</span></button> 
          </div>

          <div class="modal-body bg-body-modal">
            
               <b><label for="acantidad">Cantidad</label></b>
                <input  id='inputcantidadcarga' type="number" name="acantidad" placeholder="Cantidad adquirida" class="form-control input-sm limpiar"/>

               <div class="div-select">
                    <b><label>Proveedor: </label></b>
                   <select id="CargarseleccionProve"   name="proveedor" class="form-control">
                      <option selected="selected">Seleccione</option>
                   </select>
              </div>

                <b> <label for="adetalle">Detalle</label></b>
                <textarea name="adetalle" placeholder="Ejemplo: 'Nueva compra el dia 15/07/2018'" class="form-control input-sm limpiar" rows="2" cols="50"></textarea>

          </div>
          <div class="modal-footer bg-footer-modal">
            <button onclick='cargararticulo()' class="btn btn-success">Cargar</button>
          </div>
        </div>
      </div>
    </div>
         
<!--  FIN MODAL CARGAR ARTICULO -->


<!-- MODAL PARA DESCARGAR UN ARTICULO -->

    <div class="modal fade" id="descargararticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-cloud-download"></i> Descargar Artículo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"  onclick="limpiar()">&times;</span></button> 
          </div>

          <div class="modal-body">
            
               <b><label for="acantidad">Cantidad</label></b>
                <input type="number"  id="cantidadDescarga" name="acantidad" placeholder="Cantidad a descargar" class="form-control input-sm limpiar"/>
                               

          </div>
          <div class="modal-footer bg-footer-modal">
            <button  onclick ='descargararticulo()' class="btn btn-danger">Descargar</button>
          </div>
        </div>
      </div>
    </div>


<!--esto es para actualizar-->
         

<script  type="text/javascript">


   function actualizar(){
     //  var nombre= $("#n1").val();
         var id =<?php echo $id ?>;
        
         var nombre = $("#n1").val();
         var Precio = $("#precio").val();
         var Detalle=$("#detalle").val();
         var categoria=$("#seleccion option:selected").val();
         /*
         console.log(nombre);
         console.log(Precio);
         console.log(Detalle);
         console.log(categoria);
         */
         var arreglo ={};
         arreglo["id_articulo"]=id;

         if(nombre !=""){
         arreglo["a_nombre"]=nombre;
         }
         if(Precio != ""){
         arreglo["a_precio_venta"]=Precio;
       }
       if(Detalle != ""){
         arreglo["cantidad"]=Detalle;
       }


        /*
        if(categoria !=""){
          if($("#seleccion option:selected").val()=="0"){
             //arreglo["Categoria"]='0';
          }
          else {
          arreglo["Categoria"]=categoria;
        }
        }
        */

        console.log(arreglo);
         $("#n1").val("");
         $("#precio").val("")
         $("#detalle").val("");
         $("#seleccion").val('0');
             


        $.ajax({
            type:"POST",
            url:'http://localhost/InventarioAnvicors/Panel/EditarArticulo',
            dataType: "JSON",
            data:{
              'datos': arreglo,
              

            },
            success:function (data) {
             
              location.href= 'http://localhost/InventarioAnvicors/Panel/consultar?value='+id+'';
        

            },error:function(jqXHR, textStatus, errorThrown){
              
              console.log("error");
        }

        });
        
        // console.log(a);
      



         }


          /*Esto para descargar articulo */

         

        
</script>











<!--  FIN MODAL DESCARGAR ARTICULO -->

