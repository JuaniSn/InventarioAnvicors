<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sing extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->helper('url');
		$this->load->model('S_model');
		$this->load->library('session');
	}
	
	public function index(){
		
		if($this->session->userdata('login') ){
			redirect(base_url("panel"));
		}
		
		else if ($this->session->userdata('error')){
            $data = array('title' 		=> 'Ingresar | Sistema Anvicors',
        				  'advertencia' => 1);
			$this->load->view('requires/headlogin');
			$this->load->view('login/login', $data);
			$this->session->sess_destroy();

         }

		else if($this->session->userdata('adver')){
            $data = array('title' 		=> 'Ingresar | Sistema Anvicors',
        				  'advertencia' => 2);
			$this->load->view('requires/headlogin');
			$this->load->view('login/login', $data);
			$this->session->sess_destroy();
           
         }
        

         else {
			$data = array('title'		 => 'Inventario | Sistema Avincors', 
						 'advertencia'   =>  null);
			$this->load->view('requires/headlogin');
			$this->load->view('login/login', $data);
		}
		

	}
	

	public function ingresar(){
	
		$usuario = $this->input->post('usuario');
 		$clave   = $this->input->post('clave');
       
        
		$respuesta = $this->S_model->verificar_sesion($usuario, $clave);

		if($respuesta){
		$sesion = array( 	  'id' =>   $respuesta->id_usuario, //  $respuesta->idusuario,
							 'user' =>   $respuesta->usuario,// $respuesta->usuario,
							 'tipo' =>   $respuesta->u_tipo,//$respuesta->tipousuario,
							 'clave' =>   $respuesta->clave,//$respuesta->tipousuario,
							 'login' => TRUE);
		
			
			$this->session->set_userdata($sesion);
	     }
    	else{	
    	
    	$sesion = array('error' => 'error');	


    		$this->session->set_userdata($sesion);
		}
		
	}


}
