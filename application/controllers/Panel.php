<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 $datosprincipales;
class Panel extends CI_Controller {

	public function __construct(){
		parent:: __construct();
		$this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->library('zip');
		$this->load->library('session');
		$this->load->model('S_model');
		$this->load->library('form_validation');
	    $this->load->database();

		if (!$this->session->userdata("login")) {
			$sesion = array(  'adver' => 'Advertencia' );
			$this->session->set_userdata($sesion);				
			redirect(base_url());
		}
	}
	
	public function index(){
		$data = array(
				"cantCategorias" => $this->S_model->rowCount('categorias'),
				"cantArticulos" => $this->S_model->rowCount('articulos'),
				"cantProveedores" => $this->S_model->rowCount('proveedores')
			);
		$titulo = array('title' => 'Inicio | Sistema Anvicors');
		// print_r($data);
		$this->requiresHead($titulo);
		$this->load->view('inicio/inicio', $data);
		$this->requiresFooter();
	}


	// ESTA FUNCION VIENE DEL SIDEBAR

	public function page($parametro){

		switch ($parametro) {
	
			case 'articulos':
				$titulo = array('title' => 'Articulos | Sistema Anvicors');
				$this->requiresHead($titulo);
				$this->load->view('articulos/articulos');
				$this->requiresFooter();
			break;
			
			case 'categorias':
				$titulo = array('title' => 'Categorias | Sistema Anvicors');
				$this->requiresHead($titulo);
				$this->load->view('categorias/categorias');
				$this->requiresFooter();
			break;

			case 'proveedores':
				$titulo = array('title' => 'Proveedores | Sistema Anvicors');
				$this->requiresHead($titulo);
				$this->load->view('proveedores/proveedores');
				$this->requiresFooter();
			break;

			

			case 'usuarios':
				$titulo = array('title' => 'Usuarios | Sistema Anvicors');
				$this->requiresHead($titulo);
				$this->load->view('usuarios/usuarios');
				$this->requiresFooter();
			break;

			case 'respaldar':
				$this->load->dbutil();
				$db_format=array('format'=>'zip','filename'=>'basecels_backup.sql');
				$backup=& $this->dbutil->backup($db_format);
				$dbname='basecels-backup.zip';
				force_download($dbname,$backup);
			break;

			case 'info':
				$titulo = array('title' => 'Informacion | Sistema Anvicors');
				$this->requiresHead($titulo);
				$this->load->view('informacion/info');
				$this->requiresFooter();
			break;
			
		}

	}
    
    public function requiresHead($titulo){
		$this->load->view('requires/head', $titulo);
		$this->load->view('requires/header');
		$this->load->view('requires/sidebar');
	}

	public function requiresFooter(){
 	 	$this->load->view('requires/footer');
	}

	public function actualizarUsuario(){
		$data = trim($this->input->post('usuario'));	
		$data2 = trim($this->input->post('clave'));

		$respuesta = $this->S_model->updateUsuario($data, $data2);

		if($respuesta){
			$sesion = array('user' =>   $respuesta->usuario,
						    'clave' =>   $respuesta->clave);
			$this->session->set_userdata($sesion);
			redirect(base_url("Panel"));
		}

	}
	public function salir(){
		$this->session->sess_destroy();
		redirect(base_url());
	}



	public function agregarCategoria(){

		$this->load->model('S_model', 's_model', TRUE);
		
		header('Content-Type: application/json');

		$c_nombre 			= trim(strtoupper($this->input->post('c_nombre')));
		$c_descripcion 		= trim($this->input->post('c_descripcion'));
		$id_categoria 		= $this->input->post('id_categoria');
		
		$data = array(
				'c_nombre'         => $c_nombre,
		 		'c_descripcion'    => $c_descripcion,
		 		// 'id_categoria'	   => $id_categoria,
		 		'c_fecha_agregado' => date("Y-m-d H:i:s"),
		 		'estado' 		   => TRUE
				);
		// MANTENER ID_CATEGORIA COMENTADA SI SE ESSTA UTSANDO POSTGRE
		$respuesta = $this->s_model->insertCategoria($data);
		// print_r($respuesta);
		if($respuesta){
			echo json_encode(array('status' => TRUE));
		}
		else{		
			echo json_encode(array('status' => FALSE));
		}

	}

	public function actualizarCategoria(){
		$this->load->model('S_model', 's_model', TRUE);

		header('Content-Type: application/json');

		$c_nombre 			= trim(strtoupper($this->input->post('c_nombre')));
		$c_descripcion 		= trim($this->input->post('c_descripcion'));
		$id_categoria 		= $this->input->post('id_categoria');

		$data = array(
				'c_nombre'         => $c_nombre,
		 		'c_descripcion'    => $c_descripcion,
				);

		$respuesta = $this->s_model->updateCategoria($id_categoria, $data);

		if($respuesta){
			echo json_encode(array('status' => TRUE));
		}
		else{		
			echo json_encode(array('status' => FALSE));
		}
	}

	public function actualizarProveedor(){
		$this->load->model('S_model', 's_model', TRUE);

		$this->form_validation->set_rules('p_nombre', 'proveedor', 'required');
			$this->form_validation->set_rules('p_correo', 'correo', 'valid_email');
			$this->form_validation->set_rules('p_telefono_cel', 'celular', 'numeric');
			$this->form_validation->set_rules('p_telefono_ofi', 'oficina', 'numeric');
				
				$this->form_validation->set_message('required', 'Debes ingresar al menos un %s');
				$this->form_validation->set_message('valid_email', 'Ingrese un correo valido');
				$this->form_validation->set_message('numeric', 'Ingrese un numero de %s valido');
			
			if($this->form_validation->run() === TRUE){
				$p_nombre 			= trim(strtoupper($this->input->post('p_nombre')));
				$p_telefono_cel 	= trim($this->input->post('p_telefono_cel'));
				$p_telefono_ofi 	= trim($this->input->post('p_telefono_ofi'));
				$p_correo		 	= trim($this->input->post('p_correo'));			
				$id_proveedor 		= $this->input->post('id_proveedor');
					$data = array(
							'p_nombre'          => $p_nombre,
					 		'p_telefono_cel'    => $p_telefono_cel,
					 		'p_telefono_ofi'    => $p_telefono_ofi,
					 		'p_correo' 			=> $p_correo
							);

				$respuesta = $this->s_model->updateProveedor($id_proveedor, $data);
				print_r($respuesta);
				if($respuesta){
					echo true;				
				}
				else{		
					echo false;
				}
				
			}
			else{

				echo validation_errors("<li>","</li>");
			}
	}

	public function dataCategoria($id_categoria){
		header('Content-Type: application/json');
		$this->load->model('S_model', 's_model', TRUE);
		$data = $this->s_model->getByIdCategoria($id_categoria);

        echo json_encode($data);
	}

	public function dataProveedor($id_proveedor){
		header('Content-Type: application/json');
		$this->load->model('S_model', 's_model', TRUE);
		$data = $this->s_model->getByIdProveedor($id_proveedor);

        echo json_encode($data);
	}

	public function agregarProveedor(){
		// header('Content-Type: application/json');
		$this->load->model('S_model', 's_model', TRUE);
		
			$this->form_validation->set_rules('p_nombre', 'proveedor', 'required');
			$this->form_validation->set_rules('p_correo', 'correo', 'valid_email');
			$this->form_validation->set_rules('p_telefono_cel', 'celular', 'numeric');
			$this->form_validation->set_rules('p_telefono_ofi', 'oficina', 'numeric');
				
				$this->form_validation->set_message('required', 'Debes ingresar al menos un %s');
				$this->form_validation->set_message('valid_email', 'Ingrese un correo valido');
				$this->form_validation->set_message('numeric', 'Ingrese un numero de %s valido');
			
			if($this->form_validation->run() === TRUE){
				$p_nombre 			= trim(strtoupper($this->input->post('p_nombre')));
				$p_telefono_cel 	= trim($this->input->post('p_telefono_cel'));
				$p_telefono_ofi 	= trim($this->input->post('p_telefono_ofi'));
				$p_correo		 	= trim($this->input->post('p_correo'));			

					$data = array(
							'p_nombre'          => $p_nombre,
					 		'p_telefono_cel'    => $p_telefono_cel,
					 		'p_telefono_ofi'    => $p_telefono_ofi,
					 		'p_correo' 			=> $p_correo,
					 		'estado' 		    => TRUE
							);

				$respuesta = $this->s_model->insertProveedor($data);

				if($respuesta === TRUE){
					$var = TRUE;
					echo $var;
				}
				
			}
			else{

				echo validation_errors("<li>","</li>");
			}
		
	

		

	}

	public function listarCategoria(){
		// header('Content-Type: application/json');
		echo json_encode($this->S_model->getCat());
	}

	public function listarProveedor(){
		header('Content-Type: application/json');
		$a = $this->S_model->getProv();
	
		echo json_encode($a);
	}

	public function listarArticuloProveedor(){
		$id_proveedor = array('id_proveedor' => $this->input->post('id_proveedor'));

		$result = $this->S_model->buscarArticuloProveedor($id_proveedor);

		echo json_encode($result);
	}

	public function eliminarProveedor(){
	
		$id_proveedor =   $this->input->post('id_proveedor');	
		$respuesta = $this->S_model->dropProveedor($id_proveedor);

		if($respuesta == TRUE){
		  echo "Eliminado";
		}
		else{
		  echo "FRACASO"; 
		}

	}

	public function eliminarCategoria(){
	
		$id_categoria =   $this->input->post('id_categoria');	
		$respuesta = $this->S_model->dropCategoria($id_categoria);

		if($respuesta == TRUE){
		  echo "Eliminado";
		}
		else{
		  echo "FRACASO"; 
		}

	}


	// Funcion para el combobox categoria en la modal de articulos y tabla de categoria
	public function getSelectCategoria(){

		$estado = array('estado' => $this->input->post('estado'));

		$result = $this->S_model->selectCategoria($estado);

		echo json_encode($result);
	}

// Funcion para el combobox colores en la modal de articulos 
	public function getSelectColores(){

		$result = $this->S_model->selectColores();

		echo json_encode($result);
	}

	// Funcion para el combobox proveedor en la modal de articulos
	public function getSelectProveedor(){

		$estado = array('estado' => $this->input->post('estado'));
		$result = $this->S_model->selectProveedor($estado);

		echo json_encode($result);
	}

	// Funcion para el combobox anaqueles en la modal de articulos
	public function getSelectNroAnaquel(){

		$result = $this->S_model->selectNroAnaquel();
		$a = array(
			'A1' => $result['0'],
			'A2' => $result['10']
		);

		echo json_encode($a);
	}

	public function getSelectSecAnaquel(){

		$sec = array('nro_anaquel' => $this->input->post('nro_anaquel'));

		$result = $this->S_model->selectSecAnaquel($sec);

		if ($result['0']){
			$a = array(
			 'SEC1' => $result['0'],
			 'SEC2' => $result['3'],
			 'SEC3' => $result['6']
			);
		}
		else if ($result['9']){
			$a = array(
				'SEC1' => $result['9'],
				'SEC2' => $result['12'],
				'SEC3' => $result['15']
			);
		}
		
		echo json_encode($a);
	}

	public function getSelectColAnaquel(){

		$id_anaquel = $this->input->post('id_anaquel');
		$result = $this->S_model->selectColAnaquel();

		switch ($id_anaquel) {
			case '1':
				$a = array(
						'COL1' => $result['0'],
						'COL2' => $result['1'],
						'COL3' => $result['2']
					);
				
			break;

			case '4':
				$a = array(
						'COL1' => $result['3'],
						'COL2' => $result['4'],
						'COL3' => $result['5']
					);
				
			break;

			case '7':
				$a = array(
						'COL1' => $result['6'],
						'COL2' => $result['7'],
						'COL3' => $result['8']
					);
				
			break;
			
			case '10':
				$a = array(
						'COL1' => $result['9'],
						'COL2' => $result['10'],
						'COL3' => $result['11']
					);
				
			break;

			case '13':
				$a = array(
						'COL1' => $result['12'],
						'COL2' => $result['13'],
						'COL3' => $result['14']
					);
				
			break;

			case '16':
				$a = array(
						'COL1' => $result['15'],
						'COL2' => $result['16'],
						'COL3' => $result['17']
					);
				
			break;
		}
			

		echo json_encode($a);
	}


 /// ------------------------- codigo puesto por remberto :v ------------------------------------------------------///

	// articulos//

	public function cargarArticulosInicial(){
		header('Content-Type: application/json');
       	
    	$t=$this->S_model->getArticulos();
    	// print_r($t);
    	echo json_encode($t);
    //	prp($a);

	}




	public function vistaconsulta(){
	   header('Content-Type: application/json');
	    $datosprincipales;
	    
	    
	   $datosprincipales = $this->input->post('arreglo');
	  // $titulo = array('title' => 'Consultar Articulo | Sistema Anvicors');
	   //$vista;
	   //$a;
	   
        //$a=$this->load->view('requires/head',$titulo,true);
		//$a.=$this->load->view('requires/header',$datosprincipales,true);
		//$a.=$this->load->view('requires/sidebar',$datosprincipales,true);
		//$a=$this->load->view('requires/footer',$datosprincipales,true);
       //   $a=$this->load->view("articulos/consultar",$datosprincipales,true);
        //$a.=$this->requiresFooter();
        
       
	     
		echo json_encode($datosprincipales);
		//echo $data;
	

	}



	public function consultar(){

    $a=($this->input->get('value'));
    $resultado = $this->S_model->obtenerarticulo($a);
   	$proveedores = $this->S_model->obtenerProveedor($a);
   	// print_r($proveedores);
    $enviar=Array(
    'nombre'=> $resultado[0]['a_nombre'],
    'id'=>$resultado[0]['id_articulo'],
    'cantidad'=>$resultado[0]['a_cantidad'],
    'precio'=>$resultado[0]['a_precio_venta'],
    'categoria'=>$resultado[0]['id_categoria'],
    'nombre_categoria'=>$resultado[0]['c_nombre'],
    'descripcion'=>$resultado[0]['a_descripcion'],
    'fecha_agregado'=>$resultado[0]['a_fecha_agregado'],
    'color_articulo'=>$resultado[0]['ncolor'],
    'anaquel'=>$resultado[0]['anaquel'],
    'proveedores'  => $proveedores
    );
    // print_r($enviar);
    $titulo = array('title' => 'Consultar | Sistema Anvicors');
	$this->requiresHead($titulo);
	//$this->load->view("articulos/consultar",$enviar);
	
	$this->load->view("articulos/consultar",$enviar);
	$this->requiresFooter();
	
 
	}
	

	public function agregarArticulo(){
		$this->load->model('S_model', 's_model', TRUE);
		
		// header('Content-Type: application/json');
			$abc = $this->input->post('cboproveedor');

			$this->form_validation->set_rules('a_nombre', 'nombre', 'required');
			$this->form_validation->set_rules('a_descripcion', 'descripcion', 'required');
			$this->form_validation->set_rules('a_cantidad', 'cantidad', 'required');
			$this->form_validation->set_rules('a_precio_venta', 'precio', 'required');
			$this->form_validation->set_rules('id_categoria', 'categoria', 'required');
			$this->form_validation->set_rules('id_anaquel', 'anaquel', 'required');
			$this->form_validation->set_rules('id_proveedor', 'proveedor', 'required');
			$this->form_validation->set_rules('id_color', 'colores', 'required');
						
				
				$this->form_validation->set_message('required', 'El campo %s es requerido');



				if($this->form_validation->run() === TRUE){

				$id_categoria		= $this->input->post('id_categoria');
				$a_nombre 			= trim(strtoupper($this->input->post('a_nombre')));
				$a_descripcion 		= trim($this->input->post('a_descripcion'));
				$a_cantidad			= $this->input->post('a_cantidad');
				$a_precio_venta		= $this->input->post('a_precio_venta');
				$id_anaquel			= $this->input->post('id_anaquel');
				$id_proveedor		= $this->input->post('id_proveedor');
				$id_color			= $this->input->post('id_color');		

						$data_articulos = array(	
										'id_categoria'	   => $id_categoria,
										'a_nombre'         => $a_nombre,
								 		'a_descripcion'    => $a_descripcion,
								 		'a_fecha_agregado' => date("Y-m-d H:i:s"),
								 		'a_cantidad'	   => $a_cantidad,
								 		'a_precio_venta'   => $a_precio_venta,
								 		'id_anaquel'	   => $id_anaquel
										);

						$id_articulo = $this->s_model->insertArticulo($data_articulos);

						$data_prov_art  = array(
										'id_proveedor'     => $id_proveedor,
										'id_articulo'	   => $id_articulo
										);

						$data_color_art = array(
										'id_color'     => $id_color,
										'id_articulo'  => $id_articulo
										);

						$respuesta = $this->s_model->insertRelationArticulos($data_prov_art, $data_color_art);

						if($respuesta){
							echo true;
						}
										
			     }
			     else{

						echo validation_errors("<li>","</li>");
			    }
		
	}

	


	public function EditarArticulo(){
    header('Content-Type: application/json');
    $nuevo=$this->input->post("datos");
    $this->S_model->EditarArticulo($nuevo);
    echo json_encode($nuevo);



	}

	public function eliminarArticulo(){

		$id_articulo =   $this->input->post('datos');	
		$respuesta   =   $this->S_model->dropArticulo($id_articulo);

		if($respuesta == TRUE){
		  echo "Eliminado";
		}
		else{
		  echo "FRACASO"; 
		}

	}


    public function Cargararticulo(){
     
    
            $this->form_validation->set_rules('inputcantidadcarga', 'cantidad', 'required');
			$this->form_validation->set_rules('seleccionProve', 'proveedor', 'required');
			$this->form_validation->set_message('required', 'El campo %s es requerido');
		
			
		if($this->form_validation->run()){
			$t=array( 
			'cantidad'=>$this->input->post('inputcantidadcarga'),
            'idprovedor'=>$this->input->post('seleccionProve'),
            'idarticulo'=>$this->input->post('id_articulo'),
            'total'=>$this->input->post('total')
             );


			$a=$this->S_model->cargararticulo($t);
			if($a==true){ 
            echo true;
            }
            else {
            echo 'error';
            }

       
		}else { 

	    echo validation_errors("<li>","</li>");
		}





	 }



	 public function descargarArticulo(){
	 	$a=array(  
	    'a_cantidad'=>	$cantidad=$this->input->post('cantidad'),
	 	'id_articulo'=> $id =$this->input->post('id')
	 	);
	 	$valor =$this->S_model->descargarArticulo($a);
        if($valor){
        	echo true;
        }
        else {
        	echo false;
        }



	 }


	
 }



 